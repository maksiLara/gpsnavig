package com.epam;

import com.epam.api.GpsNavigator;
import com.epam.api.Path;


import java.io.*;
import java.util.*;

/**
 * This class app demonstrates how your implementation of {@link com.epam.api.GpsNavigator} is intended to be used.
 */
public class Main {
    private static final int START_GRAPH = 0;
    private static final int END_GRAPH = 1;
    private static final int LENGTH_GRAPH = 2;
    private static final int COST_GRAPH = 3;

    public Main() {
    }

    public static void main(String[] args) throws IOException {
        final GpsNavigator navigator = new StubGpsNavigator();
        navigator.readData("road_map.txt");

        final Path path = navigator.findPath("A", "C");
        if (path == null) {
            try {
                throw new StubGpsNavigator.MyException();
            } catch (StubGpsNavigator.MyException e) {
                e.printStackTrace();
            }
        } else {
            System.out.println(path);

        }
    }

    private static class StubGpsNavigator implements GpsNavigator {
        List<String[]> pointsAndCost = new ArrayList<>();
        ArrayList<String> vertexList;

        @Override
        public void readData(String filePath) throws IOException {
            try {
                FileInputStream fileInputStream = new FileInputStream(filePath);
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(fileInputStream));
                String line = null;
                while ((line = bufferedReader.readLine()) != null) {
                    String[] strings = line.split(" ");
                    pointsAndCost.add(new String[]{strings[START_GRAPH], strings[END_GRAPH], strings[LENGTH_GRAPH], strings[COST_GRAPH]});
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }

        @Override
        public Path findPath(String pointA, String pointB) {
            ArrayList<Path> intermediatePathList = new ArrayList<>();
            Path path = null;
            int totalCost = 0;
            boolean isCanVisited = false;
            for (String[] startEndLengthCostArray : pointsAndCost) {
                if (startEndLengthCostArray[START_GRAPH].equals(pointA)) {
                    if (startEndLengthCostArray[END_GRAPH].equals(pointB)) {
                        addToIntermediateList(intermediatePathList, startEndLengthCostArray);
                    }
                    vertexList = new ArrayList<>();
                    vertexList.add(startEndLengthCostArray[START_GRAPH]);
                    String startA = startEndLengthCostArray[END_GRAPH];
                    vertexList.add(startA);
                    while (true) {
                        for (String[] innerStartEndLengthCostArray : pointsAndCost) {
                            if (innerStartEndLengthCostArray[START_GRAPH].equals(startA)) {
                                startA = innerStartEndLengthCostArray[END_GRAPH];
                                vertexList.add(startA);
                                totalCost = getTotalCost(totalCost, innerStartEndLengthCostArray);
                                isCanVisited = true;
                            }
                        }
                        if (!isCanVisited) {
                            vertexList.clear();
                            totalCost = 0;
                            break;
                        }
                    }
                }
                path = getPath(intermediatePathList, path);
            }
            return path;
        }

        private int getTotalCost(int totalCost, String[] innerStartEndLengthCostArray) {
            int lengthParse = Integer.parseInt(innerStartEndLengthCostArray[LENGTH_GRAPH]);
            int costParse = Integer.parseInt(innerStartEndLengthCostArray[COST_GRAPH]);
            totalCost += (lengthParse * costParse);
            return totalCost;
        }

        private void addToIntermediateList(ArrayList<Path> intermediatePathList, String[] startEndLengthCostArray) {
            Path intermediatePath;
            List<String> stringList = new ArrayList<>();
            stringList.add(startEndLengthCostArray[START_GRAPH]);
            stringList.add(startEndLengthCostArray[END_GRAPH]);
            int lengthParse = Integer.parseInt(startEndLengthCostArray[LENGTH_GRAPH]);
            int costParse = Integer.parseInt(startEndLengthCostArray[COST_GRAPH]);
            intermediatePath = new Path(stringList, lengthParse * costParse);
            intermediatePathList.add(intermediatePath);
        }

        private Path getPath(ArrayList<Path> intermediatePathList, Path path) {
            if (!intermediatePathList.isEmpty()) {
                Path cheapPaths = intermediatePathList.get(0);
                for (int i = 1; i < intermediatePathList.size(); i++) {
                    Path expensivePaths = intermediatePathList.get(i);
                    if (expensivePaths.getCost() < cheapPaths.getCost()) {
                        cheapPaths = expensivePaths;
                    }
                }
                path = new Path(cheapPaths.getPath(), cheapPaths.getCost());
            }
            return path;
        }

        private static class MyException extends Throwable {
            @Override
            public void printStackTrace() {
                System.out.println("Exception");
            }
        }
    }
}
